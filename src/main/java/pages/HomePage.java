package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import tools.WebDriverUtil;

/**
 * Created by v_gehaigang on 2017/7/12.
 */
public class HomePage {
    public static WebElement SearchInput(WebDriver driver){
        return WebDriverUtil.getElement(driver,"搜索输入框","CouldMusicHomePage");
    }

    //TODO 每次需要填写driver无法只写一个

    public static void cleanSearchInput(WebDriver driver){
        HomePage.SearchInput(driver).clear();
    }

    public static void inputSearchContent(WebDriver driver,String content){
        HomePage.SearchInput(driver).sendKeys(content);
    }

    public static void doSearchAction(WebDriver driver){
        HomePage.SearchInput(driver).sendKeys(Keys.ENTER);
    }

}
