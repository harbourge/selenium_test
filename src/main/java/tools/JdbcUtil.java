package tools;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class JdbcUtil {

    private static Connection conn = null;
    private String sql;
    private static Statement stmt;

    private static final String url = "jdbc:mysql://10.114.61.18:8010/transactionsystem?"
            + "user=onlinepress_rw&password=onlinepress_123&useUnicode=true&characterEncoding=UTF8";

    public static void connect() {
        try{
            Class.forName("com.mysql.jdbc.Driver");// 动态加载mysql驱动

            System.out.println("成功加载MySQL驱动程序");
            // 一个Connection代表一个数据库连接
            conn = DriverManager.getConnection(url);

            // Statement里面带有很多方法，比如executeUpdate可以实现插入，更新和删除等
            stmt = conn.createStatement();

        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public static ResultSet sqlExecuteQuery(String sql){
        try{
            ResultSet result = stmt.executeQuery(sql);
            return result;
        }catch (Exception e){
            System.out.println("sql执行失败");
            e.printStackTrace();
        }
        return null;
    }

    public static void over(){
        try {
            if (stmt != null) {
                stmt.close();
            }
            if(conn != null){
                conn.close();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
