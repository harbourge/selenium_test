package tools;

public class LogUtil {

    public static void info(String string){
        System.out.println(string);
    }

    public static void error(String string){
        System.err.println(string);
    }

    public static void debug(String string){
        System.out.println(string);
    }
}
