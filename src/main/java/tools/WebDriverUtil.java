package tools;

import org.apache.commons.logging.Log;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static tools.ParseYamUtil.locators;
import static tools.ParseYamUtil.settings;

/**
 * Created by v_gehaigang on 2017/7/12.
 */
public class WebDriverUtil {

//    private static WebDriver driver = getWebDriver("Setting");
//    private static WebDriver driver = null;
    private static String browser_type;
    private static String site_url;

    public void WebDriverUtil(){
        //TODO 构造方法初始化
    }

    /**
     * 创建一个map动态保存webElement
     */
    private static Map<String,WebElement> webElementMap = new HashMap<>();

    /**
     * 初始化获取配置文件
     * @param settingFileName
     */
    public static void init(String settingFileName){
        LogUtil.info("init setting yaml");
        settings = ParseYamUtil.parseSettingFile(settingFileName);
        browser_type = settings.get("browser_type").get("value");
        site_url = settings.get("site_url").get("value");
    }

    /**
     * 获取driver
     * @return
     */
    public static WebDriver getWebDriver() {
        WebDriver driver = null;
        if ("chrome".equals(browser_type)) {
            driver = new ChromeDriver();
        } else if ("IE".equals(browser_type)) {
            driver = new InternetExplorerDriver();
        } else if ("fireFox".equals(browser_type)) {
            driver = new FirefoxDriver();
        } else {
//            System.out.println("not found browsertype please check browsertype");
            LogUtil.info("not fund browsertype please check browsertype");
        }
//        driver.get(site_url);
        return driver;
    }

    /**
     * 停止
     * @param driver
     */
    public static void stop(WebDriver driver){
        if(driver == null){
            System.out.println("the driver is not exist");
        }
        System.out.println("driver stop");
        driver.quit();
    }

    /**
     * start driver
     * @param driver
     */
    public static void start(WebDriver driver){
        if (driver == null){
            System.err.println("the driver is null");
        }
        System.out.println("driver start");
        driver.get(site_url);
    }

    /**
     * 获取element
     * @description 使用webElementMap作为动态库，有则直接返回，没有则搜索
     * @param key
     * @param locatorFileName
     * @return
     */
    public static WebElement getElement(WebDriver driver, String key, String locatorFileName) {
        if(driver == null){
            System.out.println("driver is not exist");
            return null;
        }
        if( webElementMap.containsKey(key)){
            return webElementMap.get(key);
        }
        locators = ParseYamUtil.parseLocatorFile(locatorFileName);
        String type = locators.get(key).get("type");
        String value = locators.get(key).get("value");
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(getBy(type, value)));
        WebElement webElement = driver.findElement(getBy(type,value));
        webElementMap.put(key,webElement);
        return webElement;
    }

    /**
     * 定位
     * @param type
     * @param value
     * @return
     */
    public static By getBy(String type, String value) {
        By by = null;
        if("xpath".equalsIgnoreCase(type)){
            by = new By.ByXPath(value);
        }else if ("id".equalsIgnoreCase(type)){
            by = new By.ById(value);
        }else if("className".equalsIgnoreCase(type)) {
            by = new By.ByClassName(value);
        }else if("cssSelector".equalsIgnoreCase(type)) {
            by = new By.ByCssSelector(value);
        }else if("linkText".equalsIgnoreCase(type)){
            by = new By.ByLinkText(value);
        }else if("name".equalsIgnoreCase(type)) {
            by = new By.ByName(value);
        }else if("tatName".equalsIgnoreCase(type)) {
            by = new By.ByTagName(value);
        }else if("partialLinkText".equalsIgnoreCase(type)) {
            by = new By.ByPartialLinkText(value);
        }else {
            System.out.println("not found the type,please check the type of selector");
        }

        return by;
    }
}

