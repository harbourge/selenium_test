package tools;

import com.sun.xml.internal.ws.api.ha.StickyFeature;
import org.ho.yaml.Yaml;
import org.xml.sax.Locator;
import sun.rmi.runtime.Log;

import java.io.File;
import java.io.FileInputStream;
import java.sql.Statement;
import java.util.HashMap;

/**
 * Created by v_gehaigang on 2017/7/12.
 */
public class ParseYamUtil {
    public static HashMap<String,HashMap<String,String>> results;
    public static HashMap<String,HashMap<String,String>> settings;
    public static HashMap<String,HashMap<String,String>> locators;
    public static HashMap<String,HashMap<String,String>> snapshot_path;
    public static HashMap<String,HashMap<String,String>> parseLocatorFile(String FileName){
        File f = new File("src/main/resources/locator/"+FileName+".yaml");
        try {
            LogUtil.info("read LocatorFile Start");
            locators = Yaml.loadType(new FileInputStream(f.getAbsolutePath()),HashMap.class);

        }catch (Exception e){
            LogUtil.info("read LocatorFile Fail");
            e.printStackTrace();
        }finally {
            LogUtil.info("read LocatorFile Finished");
        }
        return locators;
    }

    public static HashMap<String,HashMap<String,String>> parseSettingFile(String FileName){
        File f = new File("src/main/resources/config/"+FileName+".yaml");
        try {
            LogUtil.info("read SettingFile Start");
            settings = Yaml.loadType(new FileInputStream(f.getAbsolutePath()),HashMap.class);
        }catch (Exception e){
            LogUtil.info("read SettingFile Fail");
            e.printStackTrace();
        }finally {
            LogUtil.info("read SettingFile Finished");
        }
        return settings;
    }



}
