package play;

import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pages.HomePage;
import tools.PrintScreenUtil;
import tools.WebDriverUtil;

/**
 * Created by v_gehaigang on 2017/7/13.
 */
public class TestPlayMusic {
    @Test(description="搜索wild wild web歌曲并播放！")
    @Parameters("test")
    public void testSearchAndPlayMusic(String test) throws InterruptedException {

        WebDriverUtil.init("Setting");

        WebDriver webDriver = WebDriverUtil.getWebDriver();

        webDriver.manage().window().maximize();

        WebDriverUtil.start(webDriver);

        Assert.assertTrue(HomePage.SearchInput(webDriver).isDisplayed());

        HomePage.cleanSearchInput(webDriver);

        PrintScreenUtil.snapshot((TakesScreenshot) webDriver,"test");

        HomePage.inputSearchContent(webDriver,test);

        HomePage.doSearchAction(webDriver);

//        SearchResultPage.handleIFrame();
//
//        SearchResultPage.doPlayAction();

        Thread.sleep(2000);

        WebDriverUtil.stop(webDriver);
    }
}
